NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0    7:0    0   1.8G  1 loop /rofs
loop1    7:1    0  86.9M  1 loop /snap/core/4917
loop2    7:2    0  34.7M  1 loop /snap/gtk-common-themes/319
loop3    7:3    0 140.9M  1 loop /snap/gnome-3-26-1604/70
loop4    7:4    0   2.3M  1 loop /snap/gnome-calculator/180
loop5    7:5    0    13M  1 loop /snap/gnome-characters/103
loop6    7:6    0  14.5M  1 loop /snap/gnome-logs/37
loop7    7:7    0   3.7M  1 loop /snap/gnome-system-monitor/51
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0   499M  0 part 
├─sda2   8:2    0   100M  0 part 
├─sda3   8:3    0    16M  0 part 
└─sda4   8:4    0 465.2G  0 part 
sdb      8:16   1   7.6G  0 disk /cdrom
├─sdb1   8:17   1   1.8G  0 part 
└─sdb2   8:18   1   2.3M  0 part 
sdf      8:80   1   3.7G  0 disk 
└─sdf1   8:81   1   3.7G  0 part /media/ubuntu/56D6-658C
sr0     11:0    1  1024M  0 rom  
