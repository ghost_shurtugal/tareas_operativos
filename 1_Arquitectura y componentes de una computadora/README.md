Arquitectura y componentes de una computadora
----------------------------------------------------

## Nombre

Cárdenas Vallarta Josué Rodrigo

### Correo electrónico

rodrigo.cardns@gmail.com


### Liga a la descripción de la tarea

[Tarea 1: Arquitectura y componentes de una computadora][recurso-tarea]


### Resumen de los componentes del equipo

* <b>CPU</b>: Intel CORE i5-4570, 4 nucleos a 3.20 GHz
* <b>RAM</b>: 3.6 GB DDR3
* <b>Disco Duro</b>: 512 GB  SATA 3.0, 6.0 Gb/s
* <b>Tarjeta de red</b>: Intel Corporation Ethernet Connection I217-LM
* <b>Tarjeta de video</b>: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller
* <b>Dispositivo de audio</b>: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor HD Audio Controller
* <b>Entradas USB</b>: Intel Corporation 8 Series/C220 Series Chipset Family USB EHCI #1 y #2 (rev 04)
* <b>Teclado</b>
* <b>Mouse</b>

### Comandos utilizados

* El archivo [/proc/cpuinfo][out-cpuinfo] contiene la salida del comando <b>lscpu</b>, con el cual podemos obtener información de los procesadores.

* El archivo [/proc/meminfo][out-meminfo] contiene las características del estado de la memoria RAM.

* El comando [lspci][out-lspci] lista los dispositivos conectados al bus PCI, como es el caso de el dispositivo de audio, tarjetas red, de video, etc.

* El comando [lspci -v -t][out-lspci2] lista los mismos dispositivos que con [lspci][out-lspci], sólo que con la opción <b>-v</b> se despliega más información de los dispositivos y con la opción <b>-t</b> los despliega en forma de árbol.

* El comando [lspci -vv][out-lspci3] lista los mismos dispositivos que con [lspci][out-lspci], sólo que con la opción <b>-vv</b> detalla aún más la información de cada dispositivo que con la opción <b>-v</b>.

* El comando [lsusb][out-lsusb] lista a los dispositivos conectados por los puertos usb.

* El comando [lsusb -t][out-lsusb2] lista a los mismos dispositivos que con [lsusb][out-lsusb] con la diferencia de que los despliega en árbol.

* El comando [lsusb -v][out-lsusb3] lista a los mismos dispositivos que con [lsusb][out-lsusb] con la diferencia de que la información que despliega es más detallada.

* El comando [lsblk][out-lsblk] lista los discos instalados en el sistema operativo junto con sus particiones.

* El comando [lsblk --all --fs --perms --paths][out-lsblk2] lista los discos que se detectan con [lsblk][out-lsblk], sólo que con la opción <b>--all</b> lista discos vacíos que por default se omiten, con la opción <b>--fs</b> despliega información de los sistemas de archivos que manejan los discos, la opción <b>--perms</b> despliega información de los propietarios, grupos y modos de cada dispositivo y la opción <b>--path</b> muestra los paths de donde se encuentran montados los discos.

* El archivo [/proc/interrupts][out-interrupts] lista las interrupciones del sistema que están siendo utilizadas, y cuantas de cada tipo ha habido en cada procesador.

* El archivo [/proc/modules][out-modules] contiene el listado de los driversy módulos instalados en el sistema operativo.

* El archivo [/proc/misc][out-misc] contiene el listado de los drivers miselaneos en los dispositivos instalados en el sistema.

* El archivo [/proc/mounts][out-mounts] contiene el listado de dispositivos montados en el sistema, como usbs y discos externos.

* El archivo [/proc/partitions][out-partitions] contiene el listado de las particiones así como el número de bloques que contiene cada una.

* El archivo [/proc/schedstat][out-schedstat] contiene información de la calendarización de los CPUs del equipo de cómputo donde está corriendo el sistema operativo.

* El archivo [/proc/swaps][out-swaps] contiene la información del espacio utilizado por la partición swap (en este caso está vacío debido a que no se tiene una partición swap en la prueba desde la usb).

* El archivo [/proc/vmstat][out-vmstat] contiene las estadísticas de la memoria virtual del kernel.

* El comando [lshw][out-lshw] despliega la información de los dispositivos del hardware que el sistema operativo ha detectado.

* El comando [lshw -html][out-lshw2] despliega lo mismo que [lshw][out-lshw] sólo que con la opción <b>-html</b> la salida es en formato de un html.

* El comando [lshw -short][out-lshw3] despliega los mismos componentes de hardware que con [lshw][out-lshw] sólo que con la opción <b>-short</b> se despliega el path de cada dispositivo con una pequeña descripción.

* El comando [lshw -businfo][out-lshw4] despliega casi lo mismo que [lshw -short][out-lshw3], la diferencia es que en lugar del path despliega la información del bus al que se encuentra conectado.

* El comando [smartctl --scan][out-smartctl] busca los discos duros que el sistema operativo tiene accesible y lista su nombre, path y tipo.

* El comando [smartctl --all /dev/sda][out-smartctl2] imprime toda la información del disco unicado en el path que se le pasa como parámetro, muestra elementos como el número de serie, número de revoluciones por minuto, modelo, versión de ATA o SATA, etc.

* El comando [smartctl --xall /dev/sda][out-smartctl3] imprime la misma información que con [smartctl --all /dev/sda][out-smartctl2] con respecto al disco que se ubica en el path que se pasa como parámetro, sólo que añade más información y desglosa más información en algunas categorías definidas en el comando anterior.




[recurso-tarea]: https://gitlab.com/SistemasOperativos-Ciencias-UNAM/sistemasoperativos-ciencias-unam.gitlab.io/blob/master/tareas/tarea-arch.md

[out-cpuinfo]: output/proc/cpuinfo.txt
[out-meminfo]: output/proc/meminfo.txt
[out-lspci]: output/lspci.txt
[out-lspci2]: output/lspci%20-v%20-t.txt
[out-lspci3]: output/lspci%20-vv.txt
[out-lsusb]: output/lsusb.txt
[out-lsusb2]: output/lsusb%20-t.txt
[out-lsusb3]: output/lsusb%20-v.txt
[out-lsblk]: output/lsblk.txt
[out-lsblk2]: output/lsblk%20--all%20--fs%20--perms%20--paths.txt
[out-interrupts]: output/proc/interrupts.txt
[out-modules]: output/proc/modules.txt
[out-misc]: output/proc/misc.txt
[out-mounts]: output/proc/mounts.txt
[out-partitions]: output/proc/partitions.txt
[out-schedstat]: output/proc/schedstat.txt
[out-swaps]: output/proc/swaps.txt
[out-vmstat]: output/proc/vmstat.txt
[out-lshw]: output/lshw.txt
[out-lshw2]: output/lshw.html
[out-lshw3]: output/lshw%20-short.txt
[out-lshw4]: output/lshw%20-businfo.txt
[out-smartctl]: output/smartctl%20--scan.txt
[out-smartctl2]: output/smartctl%20--all.txt
[out-smartctl3]: output/smartctl%20--xall.txt
