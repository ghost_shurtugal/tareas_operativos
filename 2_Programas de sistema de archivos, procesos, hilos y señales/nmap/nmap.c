/**
 * Programa que verifica a qué puertos que acepten conexiones tcp se pueden conectar.
 */
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <pthread.h> 
#include <netdb.h>

#define PROTOCOLO "tcp"

#define N_THREADS 30

#define FROM_PORT 0
#define TO_PORT 65535

/**
 * En esta variable obtenemos el siguiente puerto que se evaluará.
 */
int port = FROM_PORT;
/**
 * Para que se pueda evaluar de manera ordenada los puertos se utilizó este mutex.
 */
pthread_mutex_t lock;

/**
 * Función que verifica si se puede comunicar a un puerto que acepta conexiones tcp.
 * @param vargp es el identificador del hilo que ejecuta la función.
 */
void *map(void *vargp) {
    // Store the value argument passed to this thread 
    int *myid = (int *) vargp, myPort;
    do {
        pthread_mutex_lock(&lock);
        myPort = port++;
        pthread_mutex_unlock(&lock);
        struct servent *servicio;

        servicio = getservbyport(myPort, PROTOCOLO);
        if (servicio) {
            printf("El hilo %d reporta que se puedo conectar al puerto %d con nombre %s.\n", myid, myPort, servicio->s_name);
        }
        endservent();
    } while (port <= TO_PORT);

}

int main(int argc, char **argv) {
    int i, n;
    pthread_t* hilos;
    if (argc == 1) {
        n = N_THREADS;
    } else {
        n = atoi(argv[1]);
    }
    hilos = malloc(sizeof (pthread_t) * n);
    printf("Listando los puertos a los que se pueden conectar por %s en este momento\n\n", PROTOCOLO);
    for (i = 0; i < n; i++) {
        pthread_create(&hilos[i], NULL, map, (void *) &hilos[i]);
    }
    pthread_exit(NULL);
    return 0;
}
