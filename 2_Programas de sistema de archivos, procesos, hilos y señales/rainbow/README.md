Rainbow
----------------------------------------------------

## Pruebas

1. Debido a que se requiere que el demonio esté corriendo, no se agregaron las pruebas al segmento run del arhicvo Makefile y se agregó una captura de pantalla mostrando su funcionamiento.

 
## Ejemplo

![rainbow][2]

- En la imagen se aprecia que el programa rainbow obtiene la cadena del archivo [a.txt][0] y está a la espera de las señales con las que va a operar.
- Al recibir la señal de <b>HUP</b> imprime el contenido de memoria y al recibir una señal <b>INT</b> cierra el programa no sin antes hacer el respaldo en el archivo binario [data_rainbow.bin][1]
- Al volver a ejecutar el programa encuentra el archivo [data_rainbow.bin][1] y con ello vuelve a poblar la memoria.



[0]: https://gitlab.com/ghost-shurtugal/tareas_operativos/blob/master/2_Programas%20de%20sistema%20de%20archivos,%20procesos,%20hilos%20y%20se%C3%B1ales/archivos_ejemplo/a.txt
[1]: https://gitlab.com/ghost-shurtugal/tareas_operativos/blob/master/2_Programas%20de%20sistema%20de%20archivos,%20procesos,%20hilos%20y%20se%C3%B1ales/data_rainbow.bin
[2]: img/1.png



