Tarea 2: Programas de sistema de archivos, procesos, hilos y señales
----------------------------------------------------

### Liga a la descripción de la tarea

[Tarea 2: Programas de sistema de archivos, procesos, hilos y señales][recurso-tarea]

## Programas

Se seleccionaron los siguientes programas:

1. [hashdeep][1]
2. [rainbow][2]
3. [nmap][3]

## Consideraciones

1. El archivo make se encuentra en este nivel del repositorio
2. Los programas se ejecutaron en este mismo nivel
3. Para compilar y ejecutar los programas se debe ocupar el comando <b>make run</b>
4. Sólo se generaron pruebas ya preparadas para el caso de hashdeep y nmap, para rainbow se anexan imágenes de la ejecución.

[recurso-tarea]:https://gitlab.com/SistemasOperativos-Ciencias-UNAM/sistemasoperativos-ciencias-unam.gitlab.io/blob/master/tareas/programas.md
[1]:https://gitlab.com/ghost-shurtugal/tareas_operativos/tree/master/2_Programas%20de%20sistema%20de%20archivos,%20procesos,%20hilos%20y%20se%C3%B1ales/hashdeep
[2]:https://gitlab.com/ghost-shurtugal/tareas_operativos/tree/master/2_Programas%20de%20sistema%20de%20archivos,%20procesos,%20hilos%20y%20se%C3%B1ales/rainbow
[3]:https://gitlab.com/ghost-shurtugal/tareas_operativos/tree/master/2_Programas%20de%20sistema%20de%20archivos,%20procesos,%20hilos%20y%20se%C3%B1ales/nmap