/**
 * Programa que ejecuta una rutina estilo hashdeep para generar las sumas md5 y sha1
 * de los archivos listados por stdin o por medio de la línea de comandos.
 */
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <pthread.h> 
#include <string.h>
#include <openssl/md5.h>
#include <openssl/sha.h>

#define MD5_DIGEST_LENGTH 16
#define SHA_DIGEST_LENGTH 20

#define MAX_NUMBER_OF_LINES 999999

/**
 * Función que obtiene la suma md5 de verificación de una cadena.
 * @param string Es la cadena de la que obtendremos la suma.
 * @param stringLength Es la longitud de la cadena pasada como parámetro.
 * @return La suma md5 de la cadena pasada como parámetro.
 */
char* getMD5(char* string, int stringLength) {
    unsigned char c[MD5_DIGEST_LENGTH];
    int i;
    char* result;
    MD5_CTX mdContext;


    MD5_Init(&mdContext);
    MD5_Update(&mdContext, string, stringLength);
    MD5_Final(c, &mdContext);
    result = malloc((MD5_DIGEST_LENGTH * 2 + 1) * sizeof (char));
    for (i = 0; i < MD5_DIGEST_LENGTH; i++) {
        sprintf(&result[i * 2], "%02x", (unsigned int) c[i]);
    }
    return result;
}

/**
 * Función que obtiene la suma sha1 de verificación de una cadena.
 * @param string Es la cadena de la que obtendremos la suma.
 * @param stringLength Es la longitud de la cadena pasada como parámetro.
 * @return La suma sha1 de la cadena pasada como parámetro.
 */
char* getSha1(char* string, int stringLength) {
    unsigned char digest[SHA_DIGEST_LENGTH];
    char* result;
    int i = 0;
    SHA_CTX ctx;
    SHA1_Init(&ctx);
    SHA1_Update(&ctx, string, stringLength);
    SHA1_Final(digest, &ctx);

    result = malloc((SHA_DIGEST_LENGTH * 2 + 1) * sizeof (char));
    for (i = 0; i < SHA_DIGEST_LENGTH; i++) {
        sprintf(&result[i * 2], "%02x", (unsigned int) digest[i]);
    }
    return result;
}

/**
 * Función que imprime las sumas md5 y sha1 de una cadena en el siguiente formato:
 * nombre	tamaño	md5	sha1
 * @param vargp Es la cadena de la cuál se quiere obtener las sumas.
 * 
 */
void *genMD5AndSha1(void *vargp) {
    char* nombreArchivo = (char*) vargp;
    char* contenido;
    int lengthName = strlen(nombreArchivo);
    long fileSize;
    FILE* fp;
    if (nombreArchivo[lengthName - 1] == '\n') {
        nombreArchivo[lengthName - 1] = '\0';
    }
    fp = fopen(nombreArchivo, "r");
    if (fp) {

        fseek(fp, 0, SEEK_END);
        fileSize = ftell(fp);
        fseek(fp, 0, SEEK_SET); //same as rewind(f);

        contenido = (char*) malloc((fileSize + 1) * sizeof (char));
        fread(contenido, sizeof (char), (fileSize + 1), fp);

        fclose(fp);
        printf("%s\t%ld\t%s\t%s\n", nombreArchivo, fileSize, getMD5(contenido, fileSize), getSha1(contenido, fileSize));
    }

}

int main(int argc, char **argv) {
    int i, stdinLinesCount = 0;
    pthread_t* hilos;

    char** stdinLines;
    size_t size;

    stdinLines = malloc(sizeof (char*) * MAX_NUMBER_OF_LINES);

    printf("Escriba el caracter '.' para terminar de recibir entradas en la terminal:\n");
    while (getline(&stdinLines[stdinLinesCount], &size, stdin) != -1) {
        if (stdinLines[stdinLinesCount][0] == '.') {
            break;
        }
        stdinLinesCount++;
    }

    hilos = malloc(sizeof (pthread_t) * (stdinLinesCount + argc - 1));
    for (i = 0; i < stdinLinesCount; i++) {
        pthread_create(&hilos[i], NULL, genMD5AndSha1, (void *) stdinLines[i]);
    }
    for (i = 1; i < argc; i++) {
        pthread_create(&hilos[i + stdinLinesCount], NULL, genMD5AndSha1, (void *) argv[i]);
    }
    pthread_exit(NULL);
    return 0;
}
