/*
 * Módulo que implementa el comando date en el kernel - /dev version
 *
 * Josué Rodrigo Cárdenas Vallarta <rodrigo.cardns@gmail.com>
 *
 */

#include <linux/module.h>       /* Needed by all modules */
#include <linux/kernel.h>       /* Needed for KERN_INFO */
#include <linux/init.h>         /* Needed for the macros */

/**
 * Función que imprime la fecha en formato de segundos.
 * return el codigo de salida que se genera.
 */
static int date_init(void) {
    unsigned get_time;
    struct timeval tv;

    do_gettimeofday(&tv);
    get_time = tv.tv_sec;
    printk(KERN_ALERT "%u\n", get_time);
    return 0;
}

/**
 * Función que se ocupa cuando se elimina el módulo.
 */
static void date_exit(void) {
    printk(KERN_ALERT "Terminado módulo");
}

module_init(date_init);
module_exit(date_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Josué Rodrigo Cárdenas Vallarta <rodrigo.cardns@gmail.com>");
MODULE_DESCRIPTION("Módulo que obtiene  la fecha en segundos desde el tiempo EPOCH de UNIX ");
MODULE_VERSION("dev");
