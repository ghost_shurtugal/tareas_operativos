Tarea 3: Módulos para el kernel Linux
----------------------------------------------------

### Liga a la descripción de la tarea

[Tarea 3: Módulos para el kernel Linux][recurso-tarea]

### Módulo seleccionado

Se eligió implementar el módulo de <b>/dev/date</b>.

[recurso-tarea]:https://gitlab.com/SistemasOperativos-Ciencias-UNAM/sistemasoperativos-ciencias-unam.gitlab.io/blob/master/tareas/linux-module.md
